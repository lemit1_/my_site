import * as React from 'react'
//import Link from 'next/link'
import Head from 'next/head'
import "../wwwroot/layout.scss"

type Props = {
  title?: string
  siteName?: string
  pagetitle?: string
}

const Layout: React.FunctionComponent<Props> = ({
  children,
  title = 'This is the default title',
  siteName="LeMIT",
  pagetitle="Главная"
}) => (
  <div>
    <Head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
        <meta http-equiv="X-UA-Compatible" content="ie=edge"></meta>
        <title>{siteName} | {title} | {pagetitle}</title>      
        <script src="https://yastatic.net/lodash/4.17.11/lodash.min.js"></script>
        <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script> 
        <script src="https://yastatic.net/jquery/3.3.1/jquery.js"></script>
        <script src="https://yastatic.net/bem-core/latest/desktop/bem-core.js"></script>
        <script src="https://yastatic.net/bem-components/latest/desktop/bem-components.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></link>
        <link rel="stylesheet" href="https://yastatic.net/bem-components/latest/desktop/bem-components.css"></link>
    </Head>
    
    <header>
      
    </header>
    {children}
    <footer className="App-footer container">
          <p>© 2019 LeMIT · <a href="/uframes">Используемые фремйворки и библиотеки</a></p>
    </footer>
  </div>
)

export default Layout
