import Link from 'next/link'
import React, {Component} from 'react';
import '../wwwroot/App.scss'
import Head from 'next/head'



const Golova = () => {
  return <Head>
  <meta charset="UTF-8"></meta>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"></meta>     
  <script src="https://yastatic.net/lodash/4.17.11/lodash.min.js"></script>
  <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script> 
  <script src="https://yastatic.net/jquery/3.3.1/jquery.js"></script>
  <script src="https://yastatic.net/bem-core/latest/desktop/bem-core.js"></script>
  <script src="https://yastatic.net/bem-components/latest/desktop/bem-components.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></link>
  <link rel="stylesheet" href="https://yastatic.net/bem-components/latest/desktop/bem-components.css"></link>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</Head>
}
export default Golova;