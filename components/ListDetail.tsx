import * as React from 'react'

import { User } from '../interfaces'

type ListDetailProps = {
  item: User
}

const ListDetail: React.FunctionComponent<ListDetailProps> = ({
  item: user,
}) => (
  <div>
    <h1>Детали о {user.name}</h1>
    <p>ID: {user.id}</p>
    <p>О нем: {user.other}</p>
  </div>
)

export default ListDetail
