import Link from 'next/link'
import React from 'react';
import { useRouter } from 'next/router'
import Golova from '../components/Golova';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Head from 'next/head';
//import '../static/About.scss'
var siteName="LeMIT"
var pagetitle="Обо мне"

function About() {
  return (
    <div className="About" lang="ru">
      <Golova/>
      <Head><title>{siteName} | {pagetitle}</title></Head>
            
      
      <Header/>
      <div className='Content'>

      </div>
      <Footer/>
    </div>
  );
}

export default About;
