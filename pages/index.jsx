import Link from 'next/link'
import React from 'react';
import '../wwwroot/App.scss'
import Head from 'next/head'
import Golova from '../components/Golova'
import Header from '../components/Header'
import Footer from '../components/Footer'
import myCarousel from '../components/myCarousel';
var siteName="LeMIT"
var pagetitle="Главная"

function App() {
  return (
    <div className="App" lang="ru">
       <Golova/>
       <Head><title>{siteName} | {pagetitle}</title></Head> 
       <Header/>

 
  <myCarousel/>

      <Footer/>
    </div>);}

export default App;
