import Link from 'next/link'
import React from 'react';
import '../wwwroot/u.scss'
import Head from 'next/head'
import Golova from '../components/Golova';
import Header from '../components/Header';
import Footer from '../components/Footer';

var siteName="LeMIT"
var pagetitle="Используемый библиотеки"

function App() {
  return (
    <div className="App" lang="ru">
      <Golova/>
      <Head><title>{siteName} | {pagetitle}</title></Head>      
      <Header/>
        <div className="Content">
        <h1>Используемые библиотеки:</h1>
            <ul class="list">            
            <li>ReactJS + ReactTypeScript</li>
            <li>NextJS + NextTypeScript</li>
            <li>Lodash</li>
            <li>NodeJS</li>
            <li>Bootstrap (JS+CSS)</li>
            <li>Node-sass</li>
            <li>Jquery</li>
            <li>Webpack</li>
            <li>TweenMax</li>
            </ul>
        </div> 
      <Footer/>
    </div>);}

export default App;