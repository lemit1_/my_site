import Link from 'next/link'
import React from 'react';
import '../static/App.scss'
import Golova from '../components/Golova';
import Header from '../components/Header';
import Footer from '../components/Footer';
var siteName="LeMIT"
var pagetitle="Загрузки"

function App() {
    return (
      <div className="App" lang="ru">
          <Golova/>
          <Head><title>{siteName} | {pagetitle}</title></Head>
           <Header/>
        <div className="Content">
        <h1>Скачать</h1>
          <ul>
              <li><a href="">Скачать программу "Обо мне"</a></li>
          </ul>
        </div>
        <Footer/>
          </div>
          );
 }         

export default App;